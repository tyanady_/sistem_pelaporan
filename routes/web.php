<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/datapelaporan', function () {
    return view('kelolapelaporan.datapelaporan');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/tambahpelaporan', function () {
    return view('kelolapelaporan.tambahpelaporan');
});

Route::get('/datauser', function () {
    return view('kelolauser.datauser');
});

Route::get('/tambahuser', function () {
    return view('kelolauser.tambahuser');
});

Route::post('loginproses','LoginController@loginPost');

Route::get('/logout', 'LoginController@logout');

