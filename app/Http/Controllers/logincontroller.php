<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\pengguna;

class logincontroller extends Controller
{
    
public function loginPost(Request $request){

		$username = $request['username'];
		$password = $request['password'];
		$akun_password = md5($password);

		$data= DB::table('user')
		->where([['USERNAME',$username],['PASSWORD',$password],])
		->get();

		// dd($data);

		if(count($data) > 0) {
        	
			foreach($data as $dt)
                {
                	Session::put('id', $dt->id);
                    Session::put('username', $dt->USERNAME);
                    Session::put('password', $dt->PASSWORD);                    
               		Session::put('login',true);

                return redirect('/dashboard');       
                }

        }if($data2 = DB::table('admin')->where([['ADMUSERNAME',$username],['ADMPASSWORD',$password]])->get()){
        	if(count($data2) > 0) {

			foreach($data2 as $dt)
                {
                	Session::put('idadmin', $dt->id);
                    Session::put('usernameadmin', $dt->ADMUSERNAME);
                    Session::put('passwordadmin', $dt->ADMPASSWORD);                    
               		Session::put('login',true);

                return redirect('/dashboard');            
                }

        }
    	}else {
				$data["status"] = "danger";
				$data["message"] = "Username atau password yang anda masukkan salah!!";
				return View('login', $data);
		}
	}

	public function logout(){
		Session::flush();
		return redirect('/');
	}

}
