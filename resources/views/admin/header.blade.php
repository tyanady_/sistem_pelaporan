 <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img align="center" width="60px" src="{{asset('images/pel3.jpg')}}" alt=" " style="display: block; margin: auto;" /></span>
      <!-- logo for regular state and mobile devices -->

    @if(Session::get('login') == true && !empty(Session::get('username')) )
         <span class="logo-lg">{{Session::get('usernameadmin')}}</span>
    @elseif(Session::get('login') == true && !empty(Session::get('usernameadmin')) )       
        <span class="logo-lg"><b>Admin</b></span>
    @endif 
      
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="{{url("logout")}}">
              Logout
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>