<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      @if(Session::get('login') == true && !empty(Session::get('username')) )
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>        
        <li>
          <a href="">
            <i class="fa fa-list-ol"></i> <span>Data Pelaporan</span>
          </a>
        </li>
        <li>
          <a href="">
            <i class="fa fa-plus"></i> <span>Tambah Data Pelaporan</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      </ul>

      @elseif(Session::get('login') == true && !empty(Session::get('usernameadmin')) )
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>        
        <li>
          <a href="">
            <i class="fa fa-list-ol"></i> <span>Data Pengguna</span>
          </a>
        </li>
        <li>
          <a href="">
            <i class="fa fa-plus"></i> <span>Tambah Pengguna</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      </ul>

      @endif
    </section>
    <!-- /.sidebar -->
  </aside>