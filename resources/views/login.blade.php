@extends('admin.master_login')
@section('content')

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href=""></a>
    <b>Sistem Pelaporan Pelanggaran</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <img align="center" width="200px" src="{{asset('images/Pelindo 3.png')}}" alt=" " style="display: block; margin: auto;" />
    @if (isset($status))
        <div class="alert alert-<?php echo $status; ?>" role="alert" >
          <?php echo $message; ?>
        </div>
        @endif
    <p class="login-box-msg"><b>Silahkan Login</b> </p>

    <form action="{{ url('loginproses') }}" method="POST">
      {{csrf_field()}}
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->
	
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>

 @endsection