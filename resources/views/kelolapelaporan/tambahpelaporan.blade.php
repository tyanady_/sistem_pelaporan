@extends('admin.admin')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Pelaporan Pelanggaran
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="">Tambah Pelaporan Pelanggaran</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Pelaporan Pelanggaran</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="">
              {{csrf_field()}}
              <div class="box-body">
              	<div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Pelaporan</label>
                  <div class="col-sm-9">
                    <input type="date" class="form-control" id="inputEmail3" name="tglpelaporan" placeholder="Tanggal Pelaporan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="nama" placeholder="Nama">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="jabatan" placeholder="Jabatan">
                  </div>
                </div> 
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Instansi</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="instansi" placeholder="Instansi">
                  </div>
                </div>   
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nomor HP</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="nomorhp" placeholder="Nomor HP">
                  </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pelaporan Via</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" name="via" >
                      <option value="">--- Pelaporan Via ---</option>

                    </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Pengaduan</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="nomorhp" placeholder="Jenis Pengaduan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Terlapor</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="terlapor" placeholder="Terlapor">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Perihal</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="3" name="Perihal" placeholder="Perihal ..."></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Lingkup</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="lingkup" placeholder="Lingkup">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">PIC</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="pic" placeholder="PIC">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Tindak Lanjut</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" name="tindaklanjut" placeholder="Tindak Lanjut">
                  </div>
                </div>             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Tambah</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection