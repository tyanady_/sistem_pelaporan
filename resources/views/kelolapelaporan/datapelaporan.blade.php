@extends('admin.admin')
@section('content')

  <style type="text/css">
    .pagination li{
      float: left;
      list-style-type: none;
      margin:5px;
    }
  </style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Pelaporan
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#">Data Pelaporan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
             <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="project-actions text-center">No</th>
                  <th class="project-actions text-center">Tanggal</th>
                  <th class="project-actions text-center">Nama</th>
                  <th class="project-actions text-center">Jabatan</th>
                  <th class="project-actions text-center">Intansi</th>
                  <th class="project-actions text-center">Nomor HP</th>
                  <th class="project-actions text-center">Jenis Pengaduan</th>
                  <th class="project-actions text-center">Lingkup</th>
                  <th class="project-actions text-center">PIC</th>
                  <th class="project-actions text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php $no=1; @endphp

                <tr>
                  <td class="project-actions text-center">{{$no}}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="project-actions text-center">
                        <div class="box-group">
                          <form action="" method="post">
                          <!-- {{csrf_field()}}
                          {{ method_field('DELETE') }} -->
                          <a href="" class="btn btn-info">Ubah</a>                        
                          <button type="submit" class="btn btn-danger">
                                  Hapus
                          </button>
                          </form>                     
                        </div>
                    </td>
                </tr>
                @php $no++; @endphp
                                
                </tbody>
              </table> 
            </div>
        </div>
        <!-- /.col -->
      </div>
    </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection